const data = require('../package.json')

export default `/*!
 * Popup.js v${data.version}
 * (c) 2018-${new Date().getFullYear()} ${data.author}
 * Released under the MIT License.
 */
`
