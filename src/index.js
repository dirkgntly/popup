import popupStore from './module/PopupStore';

let store = new popupStore();

export default class Popup {
	constructor (target, args = {}) {
		this.popup = document.querySelector(target);

		if(args.onOpen !== undefined && this.popup !== undefined) this.popup.addEventListener('popupOpen', args.onOpen);
		if(args.onClose !== undefined && this.popup !== undefined) this.popup.addEventListener('popupClose', args.onClose);
	}

	open() {
		store.openPopup(this.popup);
	}

	close() {
		store.closePopup();
	}

	onOpen(callback) {
		this.popup.addEventListener('popupOpen', callback, {once: true});
	}

	onClose(callback) {
		this.popup.addEventListener('popupClose', callback, {once: true});
	}

	get() {
		return this.popup;
	}

	static closeCurrentPopup() {
		store.closePopup();
	}
}