import bodyScrollLock from 'body-scroll-lock';

export default function PopupStore() {

	const eventOpen = new Event('popupOpen');
	const eventClose = new Event('popupClose');

	const root = document.documentElement;

	const me = this;

	let openedPopup = [];

	let defaults = {
		classRoot: '-popup',
		classOpen: '-visible',
	};

	function closeByEsc(e) {
		if (e.keyCode == 27) {
			me.closePopup();
		}
	}

	function closeByClick(e) {
		if (e.target == this) {
			me.closePopup();
		}
	}

	function removeLastPopup() {
		openedPopup.splice(-1, 1);
	}

	function getCurrentPopup() {
		return !isEmpty() ? openedPopup[openedPopup.length - 1] : -1;
	}

	function isEmpty() {
		return openedPopup.length === 0;
	}

	function isFirst() {
		return openedPopup.length === 1;
	}

	function pushPopup(element) {
		openedPopup.push(element);
	}

	this.closePopup = () => {
		if (isEmpty()) {
			return;
		}

		var popup = getCurrentPopup();

		popup.dispatchEvent(eventClose);
		popup.classList.remove(defaults.classOpen);

		popup.removeEventListener("click", closeByClick);
		bodyScrollLock.enableBodyScroll(popup);

		destroyPopup();
	}

	this.openPopup = (popup) => {
		initPopup(popup);

		popup.classList.add(defaults.classOpen);

		popup.dispatchEvent(eventOpen);
		popup.addEventListener("click", closeByClick);
		bodyScrollLock.disableBodyScroll(popup);
	}

	function initPopup(popup) {
		pushPopup(popup);

		if(isFirst()) {
			root.classList.add(defaults.classRoot);
			document.addEventListener("keyup", closeByEsc);
			if (typeof fullpage_api != 'undefined') {
				fullpage_api.setAllowScrolling(false);
			}
		}
	}

	function destroyPopup() {
		removeLastPopup();
		if (isEmpty()) {
			root.classList.remove(defaults.classRoot);
			document.removeEventListener("keyup", closeByEsc);
			if (typeof fullpage_api != 'undefined') {
				fullpage_api.setAllowScrolling(true);
			}
		}
	}
}
