import Core from '../src/index'

// Required components
import Store from '../src/module/PopupStore'
import bodyScrollLock from '/node_modules/body-scroll-lock/lib/bodyScrollLock.js';

const COMPONENTS = {
	Store,
	bodyScrollLock,
}

export default class Popup extends Core {
  mount (extensions = {}) {
    return super.mount(Object.assign({}, COMPONENTS, extensions))
  }
}