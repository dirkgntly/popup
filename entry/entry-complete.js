import Core from '../src/index'

// Required components
import Store from '../src/module/PopupStore'
import bodyScrollLock from 'body-scroll-lock';

const COMPONENTS = {
	bodyScrollLock,
	Store,
}

export default class Popup extends Core {
  mount (extensions = {}) {
    return super.mount(Object.assign({}, COMPONENTS, extensions))
  }
}
