/*!
 * Popup.js v1.0.0
 * (c) 2018-2019 dirkgntly
 * Released under the MIT License.
 */

(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.Popup = factory());
}(this, (function () { 'use strict';

	var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function unwrapExports (x) {
		return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x.default : x;
	}

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var bodyScrollLock_min = createCommonjsModule(function (module, exports) {
	!function (e, t) {
	  t(exports);
	}(commonjsGlobal, function (exports) {
	  function r(e) {
	    if (Array.isArray(e)) {
	      for (var t = 0, o = Array(e.length); t < e.length; t++) {
	        o[t] = e[t];
	      }return o;
	    }return Array.from(e);
	  }Object.defineProperty(exports, "__esModule", { value: !0 });var l = !1;if ("undefined" != typeof window) {
	    var e = { get passive() {
	        l = !0;
	      } };window.addEventListener("testPassive", null, e), window.removeEventListener("testPassive", null, e);
	  }var d = "undefined" != typeof window && window.navigator && window.navigator.platform && /iP(ad|hone|od)/.test(window.navigator.platform),
	      c = [],
	      u = !1,
	      a = -1,
	      s = void 0,
	      v = void 0,
	      f = function f(t) {
	    return c.some(function (e) {
	      return !(!e.options.allowTouchMove || !e.options.allowTouchMove(t));
	    });
	  },
	      m = function m(e) {
	    var t = e || window.event;return !!f(t.target) || 1 < t.touches.length || (t.preventDefault && t.preventDefault(), !1);
	  },
	      o = function o() {
	    setTimeout(function () {
	      void 0 !== v && (document.body.style.paddingRight = v, v = void 0), void 0 !== s && (document.body.style.overflow = s, s = void 0);
	    });
	  };exports.disableBodyScroll = function (i, e) {
	    if (d) {
	      if (!i) return void console.error("disableBodyScroll unsuccessful - targetElement must be provided when calling disableBodyScroll on IOS devices.");if (i && !c.some(function (e) {
	        return e.targetElement === i;
	      })) {
	        var t = { targetElement: i, options: e || {} };c = [].concat(r(c), [t]), i.ontouchstart = function (e) {
	          1 === e.targetTouches.length && (a = e.targetTouches[0].clientY);
	        }, i.ontouchmove = function (e) {
	          var t, o, n, r;1 === e.targetTouches.length && (o = i, r = (t = e).targetTouches[0].clientY - a, !f(t.target) && (o && 0 === o.scrollTop && 0 < r ? m(t) : (n = o) && n.scrollHeight - n.scrollTop <= n.clientHeight && r < 0 ? m(t) : t.stopPropagation()));
	        }, u || (document.addEventListener("touchmove", m, l ? { passive: !1 } : void 0), u = !0);
	      }
	    } else {
	      n = e, setTimeout(function () {
	        if (void 0 === v) {
	          var e = !!n && !0 === n.reserveScrollBarGap,
	              t = window.innerWidth - document.documentElement.clientWidth;e && 0 < t && (v = document.body.style.paddingRight, document.body.style.paddingRight = t + "px");
	        }void 0 === s && (s = document.body.style.overflow, document.body.style.overflow = "hidden");
	      });var o = { targetElement: i, options: e || {} };c = [].concat(r(c), [o]);
	    }var n;
	  }, exports.clearAllBodyScrollLocks = function () {
	    d ? (c.forEach(function (e) {
	      e.targetElement.ontouchstart = null, e.targetElement.ontouchmove = null;
	    }), u && (document.removeEventListener("touchmove", m, l ? { passive: !1 } : void 0), u = !1), c = [], a = -1) : (o(), c = []);
	  }, exports.enableBodyScroll = function (t) {
	    if (d) {
	      if (!t) return void console.error("enableBodyScroll unsuccessful - targetElement must be provided when calling enableBodyScroll on IOS devices.");t.ontouchstart = null, t.ontouchmove = null, c = c.filter(function (e) {
	        return e.targetElement !== t;
	      }), u && 0 === c.length && (document.removeEventListener("touchmove", m, l ? { passive: !1 } : void 0), u = !1);
	    } else 1 === c.length && c[0].targetElement === t ? (o(), c = []) : c = c.filter(function (e) {
	      return e.targetElement !== t;
	    });
	  };
	});
	});

	var bodyScrollLock = unwrapExports(bodyScrollLock_min);

	function PopupStore() {

		var eventOpen = new Event('popupOpen');
		var eventClose = new Event('popupClose');

		var root = document.documentElement;

		var me = this;

		var openedPopup = [];

		var defaults = {
			classRoot: '-popup',
			classOpen: '-visible'
		};

		function closeByEsc(e) {
			if (e.keyCode == 27) {
				me.closePopup();
			}
		}

		function closeByClick(e) {
			if (e.target == this) {
				me.closePopup();
			}
		}

		function removeLastPopup() {
			openedPopup.splice(-1, 1);
		}

		function getCurrentPopup() {
			return !isEmpty() ? openedPopup[openedPopup.length - 1] : -1;
		}

		function isEmpty() {
			return openedPopup.length === 0;
		}

		function isFirst() {
			return openedPopup.length === 1;
		}

		function pushPopup(element) {
			openedPopup.push(element);
		}

		this.closePopup = function () {
			if (isEmpty()) {
				return;
			}

			var popup = getCurrentPopup();

			popup.dispatchEvent(eventClose);
			popup.classList.remove(defaults.classOpen);

			popup.removeEventListener("click", closeByClick);
			bodyScrollLock.enableBodyScroll(popup);

			destroyPopup();
		};

		this.openPopup = function (popup) {
			initPopup(popup);

			popup.classList.add(defaults.classOpen);

			popup.dispatchEvent(eventOpen);
			popup.addEventListener("click", closeByClick);
			bodyScrollLock.disableBodyScroll(popup);
		};

		function initPopup(popup) {
			pushPopup(popup);

			if (isFirst()) {
				root.classList.add(defaults.classRoot);
				document.addEventListener("keyup", closeByEsc);
				if (typeof fullpage_api != 'undefined') {
					fullpage_api.setAllowScrolling(false);
				}
			}
		}

		function destroyPopup() {
			removeLastPopup();
			if (isEmpty()) {
				root.classList.remove(defaults.classRoot);
				document.removeEventListener("keyup", closeByEsc);
				if (typeof fullpage_api != 'undefined') {
					fullpage_api.setAllowScrolling(true);
				}
			}
		}
	}

	var classCallCheck = function (instance, Constructor) {
	  if (!(instance instanceof Constructor)) {
	    throw new TypeError("Cannot call a class as a function");
	  }
	};

	var createClass = function () {
	  function defineProperties(target, props) {
	    for (var i = 0; i < props.length; i++) {
	      var descriptor = props[i];
	      descriptor.enumerable = descriptor.enumerable || false;
	      descriptor.configurable = true;
	      if ("value" in descriptor) descriptor.writable = true;
	      Object.defineProperty(target, descriptor.key, descriptor);
	    }
	  }

	  return function (Constructor, protoProps, staticProps) {
	    if (protoProps) defineProperties(Constructor.prototype, protoProps);
	    if (staticProps) defineProperties(Constructor, staticProps);
	    return Constructor;
	  };
	}();

	var _extends = Object.assign || function (target) {
	  for (var i = 1; i < arguments.length; i++) {
	    var source = arguments[i];

	    for (var key in source) {
	      if (Object.prototype.hasOwnProperty.call(source, key)) {
	        target[key] = source[key];
	      }
	    }
	  }

	  return target;
	};

	var get = function get(object, property, receiver) {
	  if (object === null) object = Function.prototype;
	  var desc = Object.getOwnPropertyDescriptor(object, property);

	  if (desc === undefined) {
	    var parent = Object.getPrototypeOf(object);

	    if (parent === null) {
	      return undefined;
	    } else {
	      return get(parent, property, receiver);
	    }
	  } else if ("value" in desc) {
	    return desc.value;
	  } else {
	    var getter = desc.get;

	    if (getter === undefined) {
	      return undefined;
	    }

	    return getter.call(receiver);
	  }
	};

	var inherits = function (subClass, superClass) {
	  if (typeof superClass !== "function" && superClass !== null) {
	    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	  }

	  subClass.prototype = Object.create(superClass && superClass.prototype, {
	    constructor: {
	      value: subClass,
	      enumerable: false,
	      writable: true,
	      configurable: true
	    }
	  });
	  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
	};

	var possibleConstructorReturn = function (self, call) {
	  if (!self) {
	    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	  }

	  return call && (typeof call === "object" || typeof call === "function") ? call : self;
	};

	var store = new PopupStore();

	var Popup = function () {
		function Popup(target) {
			var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
			classCallCheck(this, Popup);

			this.popup = document.querySelector(target);

			if (args.onOpen !== undefined && this.popup !== undefined) this.popup.addEventListener('popupOpen', args.onOpen);
			if (args.onClose !== undefined && this.popup !== undefined) this.popup.addEventListener('popupClose', args.onClose);
		}

		createClass(Popup, [{
			key: 'open',
			value: function open() {
				store.openPopup(this.popup);
			}
		}, {
			key: 'close',
			value: function close() {
				store.closePopup();
			}
		}, {
			key: 'onOpen',
			value: function onOpen(callback) {
				this.popup.addEventListener('popupOpen', callback, { once: true });
			}
		}, {
			key: 'onClose',
			value: function onClose(callback) {
				this.popup.addEventListener('popupClose', callback, { once: true });
			}
		}, {
			key: 'get',
			value: function get$$1() {
				return this.popup;
			}
		}], [{
			key: 'closeCurrentPopup',
			value: function closeCurrentPopup() {
				store.closePopup();
			}
		}]);
		return Popup;
	}();

	var COMPONENTS = {
	  bodyScrollLock: bodyScrollLock,
	  Store: PopupStore
	};

	var Popup$1 = function (_Core) {
	  inherits(Popup$$1, _Core);

	  function Popup$$1() {
	    classCallCheck(this, Popup$$1);
	    return possibleConstructorReturn(this, (Popup$$1.__proto__ || Object.getPrototypeOf(Popup$$1)).apply(this, arguments));
	  }

	  createClass(Popup$$1, [{
	    key: 'mount',
	    value: function mount() {
	      var extensions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

	      return get(Popup$$1.prototype.__proto__ || Object.getPrototypeOf(Popup$$1.prototype), 'mount', this).call(this, _extends({}, COMPONENTS, extensions));
	    }
	  }]);
	  return Popup$$1;
	}(Popup);

	return Popup$1;

})));
